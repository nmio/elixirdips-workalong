defmodule ModulesExample do
  @moduledoc """
    A module used for training in [google](http://www.google.com)
  """

  @doc """
    Returns the message it is provided.
  """
  def publish(message) do
    message
  end
end
