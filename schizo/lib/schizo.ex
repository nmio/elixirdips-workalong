defmodule Schizo do
  @moduledoc """
    Silly module!
  """
  @type element_with_index :: {String.t, number}

  @doc """
    Uppercase every other word. Example

    iex> Schizo.uppercase("you are silly")
    "you ARE silly"
  """
  @spec uppercase(String.t) :: String.t
  def uppercase(string) do
    apply_every_other_word(&upcase/1, string)
  end

  @doc """
    Unvowel every other word. Example

    iex> Schizo.unvowel("you are silly")
    "you r silly"
  """
  @spec unvowel(String.t) :: String.t
  def unvowel(string) do
    apply_every_other_word(&devowel/1, string)
  end

  @spec apply_every_other_word((String.t -> String.t), String.t) :: String.t
  defp apply_every_other_word(func, string) do
    words = String.split(string)

    words_with_index = Stream.with_index(words)
    transformed_words = Enum.map(words_with_index, func)

    Enum.join(transformed_words, " ")
  end

  @spec upcase(element_with_index) :: String.t
  defp upcase(ewi) do
    transformer(ewi, &String.upcase/1)
  end

  @spec devowel(element_with_index) :: String.t
  defp devowel (ewi) do
    transformer(ewi, fn(w) -> String.replace(w, ~r/[aeiou]/, "") end)
  end

  @spec transformer(element_with_index, (String.t -> String.t)) :: String.t
  defp transformer({word, index}, transformation) do
    cond do
      rem(index, 2) == 0 -> word
      rem(index, 2) == 1 -> transformation.(word)
    end
  end

end
