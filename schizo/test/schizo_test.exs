defmodule SchizoTest do
  use ExUnit.Case
  doctest Schizo

  test "uppercase doesn't change the first word" do
    assert(Schizo.uppercase("foo") == "foo")
  end

  test "uppercase converts the second word to uppercase" do
    assert(Schizo.uppercase("foo bar") == "foo BAR")
  end

  test "uppercase converts every other word to upercase" do
    assert(Schizo.uppercase("foo bar zin vin") == "foo BAR zin VIN")
  end

  test "unvowel doesn't change the first word" do
    assert(Schizo.unvowel("foo") == "foo")
  end

  test "unvowel removes vowels from second word" do
    assert(Schizo.unvowel("foo bar") == "foo br")
  end

  test "unvowel remove every other words vowels" do
    assert(Schizo.unvowel("foo bar zin vin") == "foo br zin vn")
  end

end
